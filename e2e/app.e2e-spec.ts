import { GitlabTestPage } from './app.po';

describe('gitlab-test App', () => {
  let page: GitlabTestPage;

  beforeEach(() => {
    page = new GitlabTestPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
